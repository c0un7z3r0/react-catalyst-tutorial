import * as types from '../actions/actionTypes'
import initialState from './initialState'

export default function filterReducer(state = initialState.filters, action) {
  switch (action.type) {
    case types.TOGGLE_TODO_FILTER: {
      return {
        ...state,
        todoVisible: !state.todoVisible,
      }
    }

    case types.TOGGLE_DONE_FILTER: {
      return {
        ...state,
        doneVisible: !state.doneVisible,
      }
    }

    case types.TOGGLE_ARCHIVED_FILTER: {
      return {
        ...state,
        archivedVisible: !state.archivedVisible,
      }
    }

    default:
      return state
  }
}
