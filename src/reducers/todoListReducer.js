import * as types from '../actions/actionTypes'
import initialState from './initialState'

export default function todoListReducer(state = initialState.todoList, action) {
  switch (action.type) {
    case types.ADD_TODO: {
      return [
        ...state,
        {
          ToDoText: action.text,
          isDone: false,
          isArchived: false,
        },
      ]
    }

    case types.DELETE_TODO: {
      const index = state.findIndex((item) => item.ToDoText === action.text)
      return [...state.slice(0, index), ...state.slice(index + 1)]
    }

    case types.TOGGLE_TODO_DONE: {
      const index = state.findIndex((item) => item.ToDoText === action.text)
      return [
        ...state.slice(0, index),
        {
          ...state[index],
          isDone: !state[index].isDone,
        },
        ...state.slice(index + 1),
      ]
    }

    case types.TOGGLE_TODO_ARCHIVED: {
      const index = state.findIndex((item) => item.ToDoText === action.text)
      return [
        ...state.slice(0, index),
        {
          ...state[index],
          isArchived: !state[index].isArchived,
        },
        ...state.slice(index + 1),
      ]
    }

    default:
      return state
  }
}
