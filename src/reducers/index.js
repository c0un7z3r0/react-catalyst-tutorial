import { combineReducers } from 'redux'
import todoList from './todoListReducer'
import filters from './filterReducer'
import { routerReducer as routing } from 'react-router-redux'

const rootReducer = combineReducers({
  todoList,
  filters,
  routing,
})

export default rootReducer
