export default {
  todoList: [],
  filters: {
    todoVisible: true,
    doneVisible: true,
    archivedVisible: false,
  },
}
