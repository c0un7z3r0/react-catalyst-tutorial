import React from 'react'
import propTypes from 'prop-types'
import styles from './todoList.module'

const TodoList = ({ children }) => {
  return <ul className={styles.todoList}>{children}</ul>
}

TodoList.propTypes = {
  children: propTypes.node.isRequired,
}

export default TodoList
