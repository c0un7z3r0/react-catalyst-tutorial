import React from 'react'
import propTypes from 'prop-types'
import InputCheckbox from '@perpetualsummer/catalyst-elements/InputCheckbox'
import Button from '@perpetualsummer/catalyst-elements/Button'
import { useDispatch } from 'react-redux'
import { toggleTodoDone, toggleTodoArchived, deleteTodo } from '../../../actions/todoListActions'
import styles from './todoItem.module.scss'

const TodoItem = ({ todoData, dark }) => {
  const dispatch = useDispatch()
  const { ToDoText, isDone, isArchived } = todoData

  return (
    <li className={dark ? styles.todoListItem_dark : styles.todoListItem_light}>
      <div className={styles.checkboxContainer}>
        <InputCheckbox
          checked={isDone}
          onChange={() => dispatch(toggleTodoDone(ToDoText))}
          valid={dark}
        />
      </div>
      <p className={isDone ? styles.todoText_done : styles.todoText}>{ToDoText}</p>
      <div className={styles.deleteButtonContainer}>
        <Button
          className={styles.deleteButton}
          warning={true}
          onClick={() => dispatch(deleteTodo(ToDoText))}
        >
          <svg
            xmlns="http://www.w3.org/2000/svg"
            fill="white"
            width="40"
            height="40"
            viewBox="0 0 24 24"
          >
            <path fill="none" d="M0 0h24v24H0z"></path>
            <path fill="none" d="M0 0h24v24H0V0z"></path>
            <path d="M6 19c0 1.1.9 2 2 2h8c1.1 0 2-.9 2-2V7H6v12zm2.46-7.12l1.41-1.41L12 12.59l2.12-2.12 1.41 1.41L13.41 14l2.12 2.12-1.41 1.41L12 15.41l-2.12 2.12-1.41-1.41L10.59 14l-2.13-2.12zM15.5 4l-1-1h-5l-1 1H5v2h14V4z"></path>
          </svg>
        </Button>
      </div>
      <div className={styles.archiveButtonContainer}>
        <Button
          className={styles.archiveButton}
          warning={isArchived}
          onClick={() => dispatch(toggleTodoArchived(ToDoText))}
        >
          {isArchived ? (
            <svg
              xmlns="http://www.w3.org/2000/svg"
              fill="white"
              width="40"
              height="40"
              viewBox="0 0 24 24"
            >
              <path fill="none" d="M0 0H24V24H0z"></path>
              <path d="M20.55 5.22l-1.39-1.68A1.51 1.51 0 0018 3H6c-.47 0-.88.21-1.15.55L3.46 5.22C3.17 5.57 3 6.01 3 6.5V19a2 2 0 002 2h14c1.1 0 2-.9 2-2V6.5c0-.49-.17-.93-.45-1.28zM12 9.5l5.5 5.5H14v2h-4v-2H6.5L12 9.5zM5.12 5l.82-1h12l.93 1H5.12z"></path>
            </svg>
          ) : (
            <svg
              xmlns="http://www.w3.org/2000/svg"
              fill="white"
              width="40"
              height="40"
              viewBox="0 0 24 24"
            >
              <path fill="none" d="M0 0h24v24H0z"></path>
              <path d="M20.54 5.23l-1.39-1.68C18.88 3.21 18.47 3 18 3H6c-.47 0-.88.21-1.16.55L3.46 5.23C3.17 5.57 3 6.02 3 6.5V19c0 1.1.9 2 2 2h14c1.1 0 2-.9 2-2V6.5c0-.48-.17-.93-.46-1.27zM12 17.5L6.5 12H10v-2h4v2h3.5L12 17.5zM5.12 5l.81-1h12l.94 1H5.12z"></path>
            </svg>
          )}
        </Button>
      </div>
    </li>
  )
}

TodoItem.propTypes = {
  todoData: propTypes.shape({
    ToDoText: propTypes.string.isRequired,
    isDone: propTypes.bool.isRequired,
    isArchived: propTypes.bool.isRequired,
  }).isRequired,
  dark: propTypes.bool.isRequired,
}

export default TodoItem
