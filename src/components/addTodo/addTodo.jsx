import React, { useState } from 'react'
import { useDispatch } from 'react-redux'
import { addTodo } from '../../actions/todoListActions'
import InputTextarea from '@perpetualsummer/catalyst-elements/InputTextarea'
import FlexContainer from '@perpetualsummer/catalyst-elements/FlexContainer'
import FlexCell from '@perpetualsummer/catalyst-elements/FlexCell'
import Button from '@perpetualsummer/catalyst-elements/Button'
import styles from './addTodo.module.scss'

const AddTodo = () => {
  const dispatch = useDispatch()
  const [inputValue, setInputValue] = useState('')

  const submitTodo = () => {
    dispatch(addTodo(inputValue))
    setInputValue('')
  }

  return (
    <FlexContainer
      justifyContent="center"
      alignItems="flex-start"
      alignContent="center"
      wrap="nowrap"
      className={styles.addTodoContainer}
    >
      <FlexCell alignSelf="stretch" xs={10} sm={10} md={10} lg={10} xl={10}>
        <InputTextarea
          label="Add A To-Do"
          id="AddToDo"
          value={inputValue}
          onChange={(e) => setInputValue(e.target.value)}
          width="100%"
          rows={5}
          resize={false}
        />
      </FlexCell>
      <FlexCell alignSelf="stretch" xs={2} sm={2} md={2} lg={2} xl={2} pl={4}>
        <Button className={styles.submitButton} onClick={() => submitTodo()}>
          <svg
            xmlns="http://www.w3.org/2000/svg"
            width="50"
            height="50"
            x="0"
            y="0"
            enableBackground="new 0 0 349.03 349.031"
            viewBox="0 0 349.03 349.031"
            xmlSpace="preserve"
          >
            <path
              fill="white"
              d="M349.03 141.226v66.579a9.078 9.078 0 01-9.079 9.079H216.884v123.067a9.077 9.077 0 01-9.079 9.079h-66.579c-5.009 0-9.079-4.061-9.079-9.079V216.884H9.079A9.08 9.08 0 010 207.805v-66.579a9.079 9.079 0 019.079-9.079h123.068V9.079c0-5.018 4.069-9.079 9.079-9.079h66.579a9.078 9.078 0 019.079 9.079v123.068h123.067a9.077 9.077 0 019.079 9.079z"
            ></path>
          </svg>
        </Button>
      </FlexCell>
    </FlexContainer>
  )
}

export default AddTodo
