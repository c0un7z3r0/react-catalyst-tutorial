import React, { useContext } from 'react'
import { useHistory } from 'react-router-dom'
import FlexContainer from '@perpetualsummer/catalyst-elements/FlexContainer'
import FlexCell from '@perpetualsummer/catalyst-elements/FlexCell'
import GridContainer from '@perpetualsummer/catalyst-elements/GridContainer'
import GridCell from '@perpetualsummer/catalyst-elements/GridCell'
import Button from '@perpetualsummer/catalyst-elements/Button'
import InputSwitch from '@perpetualsummer/catalyst-elements/InputSwitch'
import { ThemeContext } from '../../context/themeContextProvider'
import styles from './settingsView.module'

const SettingsView = () => {
  const { dark, toggleThemeDark } = useContext(ThemeContext)

  const history = useHistory()
  const backToHome = () => {
    history.push('/')
  }

  return (
    <FlexContainer
      justifyContent="center"
      alignItems="flex-end"
      alignContent="center"
      wrap="wrap"
      p={0}
      className={dark ? styles.theme_dark : styles.theme_light}
    >
      <FlexCell alignSelf="stretch" xs={12} sm={10} md={8} lg={8} xl={8}>
        <GridContainer
          grid="grid"
          columns="1fr"
          rows="100px 1fr"
          gap="10px"
          style={{
            height: '100vh',
          }}
          className={styles.gridContainer}
        >
          <GridCell columnStart="1" columnEnd="1" rowStart="1" rowEnd="2">
            <header className={styles.headerContainer}>
              <h1 className={styles.headerText}>
                <strong>SETTINGS</strong>
              </h1>
              <div className={styles.closeSettingsButtonContainer}>
                <Button
                  className={styles.closeSettingsButton}
                  warning={true}
                  onClick={() => backToHome()}
                >
                  &times;
                </Button>
              </div>
            </header>
          </GridCell>
          <GridCell columnStart="1" columnEnd="1" rowStart="2" rowEnd="3" padding="0 20px">
            <InputSwitch
              label={dark ? 'Theme: Dark' : 'Theme: Light'}
              onChange={() => toggleThemeDark()}
              checked={dark}
              alert={!dark}
            />
          </GridCell>
        </GridContainer>
      </FlexCell>
    </FlexContainer>
  )
}

export default SettingsView
