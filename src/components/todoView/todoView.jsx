import React, { useContext } from 'react'
import FlexContainer from '@perpetualsummer/catalyst-elements/FlexContainer'
import FlexCell from '@perpetualsummer/catalyst-elements/FlexCell'
import GridContainer from '@perpetualsummer/catalyst-elements/GridContainer'
import GridCell from '@perpetualsummer/catalyst-elements/GridCell'
import AppHeader from '../appHeader/appHeader'
import AddTodo from '../addTodo/addTodo'
import TodoList from '../todoList/todoList'
import TodoItem from '../todoList/todoItem/todoItem'
import TodoFilters from '../todoFilters/todoFilters'
import { useSelector } from 'react-redux'
import { ThemeContext } from '../../context/themeContextProvider'
import styles from './todoView.module'

const TodoView = () => {
  const { dark } = useContext(ThemeContext)
  const todoItems = useSelector((state) => state.todoList)
  const filters = useSelector((state) => state.filters)
  const { todoVisible, doneVisible, archivedVisible } = filters

  return (
    <FlexContainer
      justifyContent="center"
      alignItems="flex-end"
      alignContent="center"
      wrap="wrap"
      p={0}
      className={dark ? styles.theme_dark : styles.theme_light}
    >
      <FlexCell alignSelf="stretch" xs={12} sm={10} md={8} lg={8} xl={8}>
        <GridContainer
          grid="grid"
          columns="1fr"
          rows="100px 1fr 150px"
          gap="10px"
          style={{
            height: '100vh',
          }}
          className={styles.gridContainer}
        >
          <GridCell columnStart="1" columnEnd="1" rowStart="1" rowEnd="2">
            <AppHeader dark={dark} />
          </GridCell>
          <GridCell columnStart="1" columnEnd="1" rowStart="2" rowEnd="3">
            <TodoFilters
              dark={dark}
              todoVisible={todoVisible}
              doneVisible={doneVisible}
              archivedVisible={archivedVisible}
            />
            <TodoList>
              {todoItems.map((itemData, i) => {
                if (itemData.isArchived === true && archivedVisible === false) {
                  return null
                } else if (
                  (itemData.isDone === false && todoVisible === true) ||
                  (itemData.isDone === true && doneVisible === true)
                ) {
                  return <TodoItem key={`todoItem_${i}`} todoData={itemData} dark={dark} />
                }
              })}
            </TodoList>
          </GridCell>
          <GridCell columnStart="1" columnEnd="1" rowStart="3" rowEnd="4">
            <AddTodo />
          </GridCell>
        </GridContainer>
      </FlexCell>
    </FlexContainer>
  )
}

export default TodoView
