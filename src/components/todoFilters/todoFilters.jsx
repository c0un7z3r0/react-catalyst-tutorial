import React from 'react'
import propTypes from 'prop-types'
import { useDispatch } from 'react-redux'
import GridContainer from '@perpetualsummer/catalyst-elements/GridContainer'
import GridCell from '@perpetualsummer/catalyst-elements/GridCell'
import InputSwitch from '@perpetualsummer/catalyst-elements/InputSwitch'
import {
  toggleTodoFilter,
  toggleDoneFilter,
  toggleArchivedFilter,
} from '../../actions/filterActions'
import styles from './todoFilters.module'

const TodoFilters = ({ dark, todoVisible, doneVisible, archivedVisible }) => {
  const dispatch = useDispatch()

  return (
    <GridContainer grid="grid" columns="1fr 1fr 1fr" rows="64px" gap="10px" padding="0 10px 10px">
      <GridCell
        columnStart="1"
        columnEnd="2"
        className={dark ? styles.filterCell_dark : styles.filterCell_light}
      >
        <InputSwitch
          label="To Do"
          name="InputName"
          onChange={() => dispatch(toggleTodoFilter())}
          checked={todoVisible}
          valid={todoVisible}
          warning={!todoVisible}
        />
      </GridCell>
      <GridCell
        columnStart="2"
        columnEnd="3"
        className={dark ? styles.filterCell_dark : styles.filterCell_light}
      >
        <InputSwitch
          label="Done"
          name="InputName"
          onChange={() => dispatch(toggleDoneFilter())}
          checked={doneVisible}
          valid={doneVisible}
          warning={!doneVisible}
        />
      </GridCell>
      <GridCell
        columnStart="3"
        columnEnd="4"
        className={dark ? styles.filterCell_dark : styles.filterCell_light}
      >
        <InputSwitch
          label="Archived"
          name="InputName"
          onChange={() => dispatch(toggleArchivedFilter())}
          checked={archivedVisible}
          valid={archivedVisible}
          warning={!archivedVisible}
        />
      </GridCell>
    </GridContainer>
  )
}

TodoFilters.propTypes = {
  dark: propTypes.bool.isRequired,
  todoVisible: propTypes.bool.isRequired,
  doneVisible: propTypes.bool.isRequired,
  archivedVisible: propTypes.bool.isRequired,
}

export default TodoFilters
