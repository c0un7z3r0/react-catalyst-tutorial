// WEB BUILD Entry point
// Used for both 'dev' mode and 'production' mode builds
import React from 'react'
import { render } from 'react-dom'

// APPLICATION ROOT COMPONENT
import App from './app'

// REACT ROUTER DOM
import { BrowserRouter } from 'react-router-dom'

// REDUX
import { Provider } from 'react-redux'
import configureStore from './store/configureStore'
import { saveState } from './store/localStorage'
const store = configureStore()

store.subscribe(() => {
  saveState(store.getState())
})

// CONTEXT
import ViewportContextProvider from './context/viewportContextProvider'
import ThemeContextProvider from './context/themeContextProvider'

render(
  <Provider store={store}>
    <BrowserRouter>
      <ViewportContextProvider>
        <ThemeContextProvider>
          <App />
        </ThemeContextProvider>
      </ViewportContextProvider>
    </BrowserRouter>
  </Provider>,
  document.getElementById('application')
)
