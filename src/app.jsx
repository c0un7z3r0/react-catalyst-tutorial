import { hot } from 'react-hot-loader/root'
import React from 'react'
import { Switch, Route } from 'react-router-dom'

import TodoView from './components/todoView/todoView'
import SettingsView from './components/settingsView/settingsView'

import './public/css/normalize.css'
import './public/css/scrollbars.scss'
import './public/css/typography.scss'
import './global.scss'

const App = () => {
  return (
    <Switch>
      <Route exact path="/" component={TodoView} />
      <Route exact path="/settings" component={SettingsView} />
    </Switch>
  )
}

export default hot(App)
