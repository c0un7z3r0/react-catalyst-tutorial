import * as types from './actionTypes'

export function toggleTodoFilter() {
  return {
    type: types.TOGGLE_TODO_FILTER,
  }
}

export function toggleDoneFilter() {
  return {
    type: types.TOGGLE_DONE_FILTER,
  }
}

export function toggleArchivedFilter() {
  return {
    type: types.TOGGLE_ARCHIVED_FILTER,
  }
}
