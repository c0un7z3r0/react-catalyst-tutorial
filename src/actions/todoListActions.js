import * as types from './actionTypes'

export function addTodo(todoText) {
  return {
    type: types.ADD_TODO,
    text: todoText,
  }
}

export function deleteTodo(todoText) {
  return {
    type: types.DELETE_TODO,
    text: todoText,
  }
}

export function toggleTodoDone(todoText) {
  return {
    type: types.TOGGLE_TODO_DONE,
    text: todoText,
  }
}

export function toggleTodoArchived(todoText) {
  return {
    type: types.TOGGLE_TODO_ARCHIVED,
    text: todoText,
  }
}
