import React, { useState, useLayoutEffect } from 'react'
import propTypes from 'prop-types'

export const ThemeContext = React.createContext({
  dark: false,
  toggleThemeDark: () => {},
})

const ThemeContextProvider = ({ children }) => {
  const [dark, setDark] = useState(window.localStorage.getItem('darkTheme'))

  const toggleThemeDark = () => {
    setDark(!dark)
    window.localStorage.setItem('darkTheme', !dark)
  }

  useLayoutEffect(() => {
    const lastTheme = window.localStorage.getItem('darkTheme')
    if (lastTheme === 'true') {
      setDark(true)
    }
    if (!lastTheme || lastTheme === 'false') {
      setDark(false)
    }
  }, [dark])

  return (
    <ThemeContext.Provider
      value={{
        dark,
        toggleThemeDark,
      }}
    >
      {children}
    </ThemeContext.Provider>
  )
}
ThemeContextProvider.propTypes = {
  children: propTypes.node.isRequired,
}
export default ThemeContextProvider
