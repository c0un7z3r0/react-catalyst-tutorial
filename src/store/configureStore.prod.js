import { createStore, applyMiddleware, compose } from 'redux'
import rootReducer from '../reducers'
import thunk from 'redux-thunk'
import { loadState } from './localStorage'

export default function configureStore(initialState) {
  const persistedState = loadState()
  return createStore(
    rootReducer,
    persistedState ? persistedState : initialState,
    compose(
      applyMiddleware(thunk),
      window.__REDUX_DEVTOOLS_EXTENSION__ ? window.__REDUX_DEVTOOLS_EXTENSION__() : (f) => f
    )
  )
}
