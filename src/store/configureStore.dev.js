import { createStore, applyMiddleware, compose } from 'redux'
import rootReducer from '../reducers'
import reduxImmutableStateInvariant from 'redux-immutable-state-invariant'
import thunk from 'redux-thunk'
import { loadState } from './localStorage'

export default function configureStore(initialState) {
  const persistedState = loadState()
  const store = createStore(
    rootReducer,
    persistedState ? persistedState : initialState,
    compose(
      applyMiddleware(thunk, reduxImmutableStateInvariant()),
      window.__REDUX_DEVTOOLS_EXTENSION__ && window.__REDUX_DEVTOOLS_EXTENSION__()
      // window.__REDUX_DEVTOOLS_EXTENSION__ ? window.devToolsExtension() : (f) => f
      // window.devToolsExtension ? window.devToolsExtension() : (f) => f
    )
  )

  if (module.hot) {
    // Enable Webpack hot module replacement for reducers
    module.hot.accept('../reducers', () => {
      const nextRootReducer = require('../reducers/index')
      store.replaceReducer(nextRootReducer)
    })
  }

  return store
}
